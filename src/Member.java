
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Member {


    private String name;
    private Date birthday;
    private String address;
    private String phone;
    private int deposit;

    public Member(String name, Date birthday, String address, String phone, int deposit) {
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.deposit = deposit;
    }


    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public float getDeposit() {
        return deposit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    @Override
    public String toString() {

        DateFormat birthDate = new SimpleDateFormat("yyyy-mm-dd");
        String mBirthDate = birthDate.format(birthday);

        return String.format("姓名：%-10s 生日:%-12s 電話:%-12s 住址:%-8s 存款:%-10s"
                ,name,mBirthDate,phone,address,deposit);
    }
}
