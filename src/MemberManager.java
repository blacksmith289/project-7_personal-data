import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MemberManager {

    private List<Member> members = new ArrayList<>();
    private String path = "member.txt";

    //Load Data
    public void loadData(){


        try {
            /**
             * 1. Define file path (get file)
             * 2. Define file to read (get fr)
             * 3. Define the file which was read to the buffer (get br)
             * 4. Define an empty String to catch what was in th buffer (get line)
             * 5. Define a StringBuilder to attach the data from "br"
             * 6. Make line as what was read, and if isn't null, add a "@" at the end to distinguish
             * 7. close fr and br
             * 8. Set the Data
             **/

            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String line;
            StringBuilder rawString = new StringBuilder();

            while ((line = br.readLine())!=null){
                rawString.append(line+"@");
            }

        br.close();
        fr.close();

        setData(rawString.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setData(String rawString){

        /**
         * 1. Define a data set to get the split data from the raw line
         * 2. Define a temporary data (temp data) set to get categorize the detail part
         * 3. Define the Data Format to save
         * 4. Define the constructor, add the parameters of temp data
         * 5. Add the data to the array list
         * **/

        //divide raw line to multiple raw single lines
        String sortData[] = rawString.split("@");
        for(int i=0; i<sortData.length;i++){
            //divide raw single line to sort single line
            String tempData[] = sortData[i].split(",");

            SimpleDateFormat sf = new SimpleDateFormat("yyyy/mm/dd");
            try {
                Date mDate = sf.parse(tempData[1]);
                Member mMember = new Member(tempData[0],mDate,tempData[2],tempData[3],Integer.parseInt(tempData[4]));
                members.add(mMember);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
    public void showData(){

        for(int i=0;i<members.size();i++){
            System.out.println(members.get(i).toString());
        }

    }
    public void sortByName(){
        Collections.sort(members, new Comparator<Member>() {
            @Override
            public int compare(Member o1, Member o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        showData();
    }
    public void sortByBirth(){
        Collections.sort(members, new Comparator<Member>() {
            @Override
            public int compare(Member o1, Member o2) {
                return o2.getBirthday().compareTo(o1.getBirthday());
            }
        });
        showData();
    }
    public void sortByDeposit(){
        Collections.sort(members, new Comparator<Member>() {
            @Override
            public int compare(Member o1, Member o2) {
                return Math.round(o2.getDeposit()-o1.getDeposit());
            }
        });
        showData();
    }

}
